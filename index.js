//
//  Load required libraries
//

var express = require("express");
var path = require("path");

var app = express();

var imgUrl = [  "/picture/01.jpg",
                "/picture/02.jpg", 
                "/picture/03.jpg",
                "/picture/04.jpg",
                "/picture/05.jpg",
                "/picture/06.jpg"   ];

// Set port: argument passed in OR environment variable OR default port 3000
app.set("port", parseInt(process.argv[2]) || process.env.APP_PORT || 3000);


// Look for all the images in the image folder
app.use("/picture", express.static(__dirname + "/images"));

// If it gets to this route, it means that the route above had failed
app.use(function(req, resp) {
    resp.status(404);
    resp.type("text/html"); // Representation of the resource
    resp.send("<img src = '" + imgUrl[parseInt(Math.random() * imgUrl.length)] + "' width=100%>");
});


// Start app
app.listen(app.get("port"), function() {
    console.info(">>> Application is listning on port " + app.get("port"));
})
